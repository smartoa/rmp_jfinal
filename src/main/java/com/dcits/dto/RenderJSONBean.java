package com.dcits.dto;

import java.io.Serializable;

/**
 * 以JSON格式将内容返回给前台的封装对象
 * @author xuwangcheng
 * @version 2018.2.5
 *
 */
public class RenderJSONBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 返回码,0为正常,其他为异常
	 */
	private Integer returnCode = 0;
	private Integer code = 0;
	private Integer count = 0;


	/**
	 * 返回说明,失败或者异常时前台展示内容
	 */
	private String msg = "";
	
	/**
	 * 正常情况下返回的数据
	 */
	private Object data;
	
	
	
	public RenderJSONBean() {
		super();

	}

	public RenderJSONBean(Integer returnCode, String msg, Object data) {
		super();
		this.returnCode = returnCode;
		this.msg = msg;
		this.data = data;
	}
	public RenderJSONBean(Integer returnCode, String msg, Object data,Integer count) {
		super();
		this.returnCode = returnCode;
		this.code = returnCode;
		this.msg = msg;
		this.data = data;
		this.count = count;
	}

	public void setReturnCode(Integer returnCode) {
		this.returnCode = returnCode;
	}
	
	public Integer getReturnCode() {
		return returnCode;
	}
	
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public String getMsg() {
		return msg;
	}
	
	public void setData(Object data) {
		this.data = data;
	}
	
	public Object getData() {
		return data;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
}
