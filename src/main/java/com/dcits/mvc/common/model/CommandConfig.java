package com.dcits.mvc.common.model;

import com.dcits.mvc.common.model.base.BaseCommandConfig;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class CommandConfig extends BaseCommandConfig<CommandConfig> {

    public static final CommandConfig dao = new CommandConfig().dao();

    public static final String TABLE_NAME = "rmp_command_config";

    public static final String column_id = "id";

    public static final String column_config_id = "configId";

    public static final String column_brief_command = "briefCommand";

    public static final String column_command = "command";

    public static final String column_use_flag = "useFlag";

    public static final String column_mark = "mark";

    public static final String column_create_time = "createTime";


}
