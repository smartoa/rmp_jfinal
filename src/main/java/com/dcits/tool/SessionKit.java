package com.dcits.tool;

import javax.servlet.http.HttpSession;

/**
 * @author xuwangcheng
 * @version 1.0.0
 * @description
 * @date 2020/3/8 3:06
 */
public class SessionKit {
    private static ThreadLocal tl = new ThreadLocal<HttpSession>();

    public static void put(HttpSession s) {
        tl.set(s);
    }

    public static HttpSession get() {
        return (HttpSession) tl.get();
    }

    public static void remove() {
        tl.remove();
    }
}
