package com.dcits.tool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolUtil {
	
	private static final ExecutorService pool = Executors.newFixedThreadPool(180);
	
	public static void execThread(Runnable exec) {
		pool.execute(exec);
	}
	
	public static void shutdownPool() {
		pool.shutdown();
	}
}
