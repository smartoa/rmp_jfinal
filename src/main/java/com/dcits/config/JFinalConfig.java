package com.dcits.config;

import java.util.HashMap;

import com.dcits.mvc.common.controller.*;
import com.jfinal.ext.handler.UrlSkipHandler;
import com.jfinal.plugin.cron4j.Cron4jPlugin;
import org.apache.log4j.Logger;

import com.dcits.business.server.ServerType;
import com.dcits.business.userconfig.UserSpace;
import com.dcits.constant.ConstantGlobalAttributeName;
import com.dcits.constant.ConstantInit;
import com.dcits.interceptor.GlobalInterceptor;
import com.dcits.mvc.common.model._MappingKit;
import com.dcits.tool.ThreadPoolUtil;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.Controller;
import com.jfinal.core.JFinal;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.template.Engine;

public class JFinalConfig extends com.jfinal.config.JFinalConfig {


    private static final Logger logger = Logger.getLogger(JFinalConfig.class);

    /**
     * 配置常量
     */
    @Override
    public void configConstant(Constants me) {

        logger.info("加载init.properties配置文件...");
        PropKit.use("init.properties");
        logger.info("Dev_Mode=" + PropKit.getBoolean(ConstantInit.config_devMode, false));
        me.setDevMode(PropKit.getBoolean(ConstantInit.config_devMode, false));
    }

    public static DruidPlugin createDruidPlugin() {
        return new DruidPlugin(PropKit.get(ConstantInit.db_connection_jdbc_url), PropKit.get(ConstantInit.db_connection_username),
                PropKit.get(ConstantInit.db_connection_password).trim(), PropKit.get(ConstantInit.db_connection_driver_class));
    }

    /**
     * 配置路由
     */
    @Override
    public void configRoute(Routes me) {

        me.add("/server", ServerInfoController.class);
        me.add("/config", UserConfigController.class);
        me.add("/report", FileInfoController.class);
        me.add("/sso", SSOApiController.class);
        me.add("/command", CommandConfigController.class);
        logger.info("加载server.properties配置文件...");
        ServerType.setAllServerType();

        for (String typeName : ServerType.controllerClasses.keySet()) {
            me.add("/" + typeName, (Class<Controller>) ServerType.controllerClasses.get(typeName));
        }


    }

    @Override
    public void afterJFinalStart() {

        JFinal.me().getServletContext().setAttribute(ConstantGlobalAttributeName.USER_SPACE_ATTRITURE_NAME,
                new HashMap<String, UserSpace>());

    }


    @Override
    public void beforeJFinalStop() {

        ThreadPoolUtil.shutdownPool();
    }

    @Override
    public void configEngine(Engine me) {


    }

    /**
     * 配置插件
     */
    @Override
    public void configPlugin(Plugins me) {

        logger.info("加载Druid数据连接池插件...");
        //配置数据连接池
        DruidPlugin druidPlugin = createDruidPlugin();
        druidPlugin.set(PropKit.getInt(ConstantInit.db_initialSize), PropKit.getInt(ConstantInit.db_minIdle),
                PropKit.getInt(ConstantInit.db_maxActive));
        me.add(druidPlugin);
        logger.info("加载ActiveRecordPlugin插件...");
        ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
        //arp.setShowSql(true);

        _MappingKit.mapping(arp);
        me.add(arp);
        //直接配置cron4j
//        Cron4jPlugin cp = new Cron4jPlugin(PropKit.use("task.txt"));
//        me.add(cp);

    }

    @Override
    public void configInterceptor(Interceptors me) {

        me.addGlobalActionInterceptor(new GlobalInterceptor());
    }

    @Override
    public void configHandler(Handlers me) {
        me.add(new UrlSkipHandler("^/websocket", false));
    }

}
