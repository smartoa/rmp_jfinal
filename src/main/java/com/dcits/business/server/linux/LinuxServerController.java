package com.dcits.business.server.linux;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.dcits.business.server.BaseServerController;
import com.dcits.business.server.ServerType;
import com.dcits.business.server.ViewServerInfo;
import com.dcits.business.userconfig.UserSpace;
import com.dcits.constant.ConstantReturnCode;
import com.dcits.mvc.common.model.CommandConfig;
import com.dcits.mvc.common.model.ServerInfo;
import com.dcits.mvc.common.service.CommandConfigService;
import com.dcits.mvc.common.service.ServerInfoService;
import com.dcits.tool.StringUtils;
import com.dcits.tool.ssh.SSHUtil;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class LinuxServerController extends BaseServerController {
    public static final Logger logger = Logger.getLogger(LinuxServerController.class);
    private ServerInfoService serverInfoService = new ServerInfoService();
    private static CommandConfigService commandService = new CommandConfigService();

    /**
     * 中断命令执行
     */
    public void breakCommand() {
        SSHUtil.stopExecCommand(getPara("tag"));
        renderSuccess(null, "成功发送停止命令!");
    }

    /**
     * 执行命令
     */
    public void execCommand() {
        String userKey = getPara("userKey");
        UserSpace space = UserSpace.getUserSpace(userKey);
        String ids = getPara("viewIds");
        String linuxType = getPara("type");
        int configId = UserSpace.getUserSpace(userKey).getUserConfig().getId();
        final String tag = getPara("tag");
        final String command = getPara("command");

        List<CommandConfig> list = commandService.findCommandByBriefCommand(command.trim(), "1", configId);
        final String briefCommand = (list.size() > 0 && "linux".equals(linuxType)) ? list.get(0).getCommand() : "";

        final JSONArray returnArr = new JSONArray();

        for (String id : ids.split(",")) {

            final LinuxServer server = "linux".equals(linuxType) ? (LinuxServer) space.getServerInfo(userKey, Integer.valueOf(id)) :
                    (LinuxServer) space.getServerInfo(LinuxServer.SERVER_TYPE_NAME, Integer.valueOf(id));
            if (server == null) continue;
            final JSONObject obj = new JSONObject();
            if ("linux".equals(linuxType)) {
                obj.put("host", server.getHost() + ":" + id);
            } else {
                obj.put("host", (StringUtils.isEmpty(server.getRealHost()) ? server.getHost() : server.getRealHost()) + "[" + server.getTags() + "]");
            }


            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {

                    try {
                        long begin = System.currentTimeMillis();
                        String returnInfo = SSHUtil.execCommand(server.getConn(), StringUtils.isEmpty(briefCommand) ? command : briefCommand, 999999, 2, tag);
                        long end = System.currentTimeMillis();
                        obj.put("returnInfo", returnInfo);
                        obj.put("useTime", (end - begin));
                    } catch (Exception e) {

                        logger.error(obj.get("host").toString() + " 命令执行失败:" + command, e);
                        obj.put("returnInfo", "命令执行失败:" + e.getMessage());
                        obj.put("useTime", 0);
                    }
                    returnArr.add(obj);
                }
            });
            t.start();
            try {
                t.join();
            } catch (InterruptedException e) {
                logger.error(e);
            }
        }

        renderSuccess(returnArr, "命令执行成功!");
		
/*		try {
			long begin = System.currentTimeMillis();
			String returnInfo = SSHUtil.execCommand(server.getConn(), getPara("command"), 999999, 2, getPara("tag"));
			long end = System.currentTimeMillis();
			renderSuccess(setData("returnInfo", returnInfo).setData("useTime", (end - begin)), "命令执行成功!");
		} catch (Exception e) {

			logger.error(server.getHost() + ":" + server.getPort() + "命令执行失败:" + getPara("command"), e);
			renderError(ConstantReturnCode.SYSTEM_ERROR, "命令执行失败:" + e.getMessage());
		}*/
    }

    /**
     * 检查当前主机上的java进程号
     */
    public void checkJps() {
        UserSpace space = UserSpace.getUserSpace(getPara("userKey"));
        LinuxServer linux = (LinuxServer) space.getServerInfo(LinuxServer.SERVER_TYPE_NAME, getParaToInt("viewId"));

        String javaHome = getPara("javaHome");
        if (StringUtils.isNotEmpty(linux.getParameters()) && StringUtils.isEmpty(javaHome)) {
            LinuxExtraParameter parameter = JSONObject.parseObject(linux.getParameters(), new TypeReference<LinuxExtraParameter>() {
            });
            javaHome = parameter.getJavaHome();
        }

        if (StringUtils.isEmpty(javaHome) && "root".equalsIgnoreCase(linux.getUsername())) {
            javaHome = linux.parseJavaHome();
        }

        String processes = "";
        try {
            processes = linux.checkJps(javaHome);
        } catch (Exception e) {

            processes = "执行jps命令出错,请检查javaHome是否设置正确:" + e.getMessage();
        }

        renderSuccess(setData("viewId", linux.getViewId()).setData("javaHome", javaHome)
                        .setData("processes", StringUtils.isEmpty(processes) ? "没有查询到该主机上的java进程列表,请检查javaHome是否设置正确或者当前账号的权限" : processes)
                , "加载成功!");
    }

    public void getLinuxConnects() throws Exception {
        String ids = getPara("ids");

        if (StringUtils.isEmpty(ids)) {
            renderError(ConstantReturnCode.SERVER_CONNECT_FAILED, "请选择连接的服务器");
            return;
        }
        String userKey = getPara("userKey");
        UserSpace space = UserSpace.getUserSpace(userKey);
        Map<String, List<ViewServerInfo>> userKeyList = space.getServers();
        List<ViewServerInfo> linuxServers = userKeyList.get(userKey);
        if (linuxServers ==null) {
            linuxServers = new ArrayList<>();
        }
        List<ViewServerInfo> connectServers = new ArrayList<>();
        String[] idArray = ids.split(",");
        for (String id : idArray) {
            ServerInfo info = serverInfoService.findById(Integer.valueOf(id));
            if (info == null) {
                continue;
            }
            LinuxServer serverInfo = (LinuxServer) ServerType.typeClasses.get(info.getServerType()).newInstance();
            serverInfo.setBaseServerInfo(info);
            String flag = serverInfo.connectssh();

            if (!"true".equals(flag)) {
                continue;
            }
            connectServers.add(serverInfo);
            linuxServers.add(serverInfo);
            //更新最后一次使用时间
            serverInfoService.updateLastTime(info);
        }
        // 将连接好的服务器，存放到全局列表中
        userKeyList.put(userKey, linuxServers);
        String msg = idArray.length == connectServers.size() ? "连接成功" : "连接成功" + connectServers.size() + "台，连接失败" + (idArray.length - connectServers.size()) + "台";
        renderSuccess(JSONObject.toJSON(connectServers), msg);
    }



    public void disConnect() {
        String userKey = getPara("userKey");
        UserSpace space = UserSpace.getUserSpace(userKey);
        String ids = getPara("ids");
        List viwds = new ArrayList();

        for (String id : ids.split(",")) {
            LinuxServer server = (LinuxServer) space.getServerInfo(getPara("userKey"), Integer.valueOf(id));
            if (server == null) continue;
            server.disJschSession();
            space.delServerInfo(userKey, Integer.valueOf(id));
        }
        renderSuccess(JSONObject.toJSON(viwds), "");
    }

    public void reConnect() {
        String userKey = getPara("userKey");
        UserSpace space = UserSpace.getUserSpace(userKey);
        String ids = getPara("ids");

        String[] idArray = ids.split(",");
        List viwds = new ArrayList();

        for (String id : idArray) {
            LinuxServer server = (LinuxServer) space.getServerInfo(userKey, Integer.valueOf(id));
            try {
                server.disJschSession();
                String flag = server.connectssh();
                if (!"true".equals(flag)) {
                    continue;
                }
                viwds.add(server.getHost() + ":" + server.getViewId());
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }
        renderSuccess(JSONObject.toJSON(viwds), "");
    }


}
